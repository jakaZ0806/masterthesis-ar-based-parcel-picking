\section{Evaluation Execution}
\label{sec:live-test}
This section describes the execution of the evaluation. At first, in section \ref{subsec:evaluation-steps}, each step of the evaluation process is described and a reason is given why it is performed. In section \ref{subsec:evaluation-data}, the data recorded during these evaluation steps is listed. Finally, in section \ref{subsec:evaluation-variations} the varying parameters of the evaluation are described.

\subsection{Evaluation Steps}
\label{subsec:evaluation-steps}
At first, before the evaluation can begin, a scenario has to be created. This is done by choosing possible target packages and their positions and placing them accordingly. The data of the package's positions, measurements and colors is stored in the packageData class, which is described in chapter \ref{chap:concept} in the implementation section \ref{sec:implementation}. After the scenario is created and the data is entered, a base line for the times that the evaluation can be compared to has to be set. In order for it to not get as repetetive and the user is not able to memorize all number, each package is labelled with three numbers in different colors to choose from.
For the test, a package number and a color is randomly chosen. The package that fits the combination is then located manually, without the help of the application. As soon as the package is found, the time the search took is logged. This is repeated several times. The average time it took to find each package is the time the application has to compare to. This cannot be repeated indefinitely because the user will eventually remember where each package is located. Due to the fact that each package has three numbers and the increase in complexity this brings, remembering is not as easy. This ensures that an actual search is performed every time instead of remembering where a specific package number is located.\newline
After the base line is set, the actual evaluation using the application can be performed. For the evaluation, the following steps are necessary. Each of these steps is performed multiple times throughout each iteration.
\begin{enumerate}
    \item{Start the Application *}
    \item {Detect the markers *}
    \item {Application chooses package randomly}
    \item {Look for the package}
    \item {Manually pick a package according to position marked *}
    \item {Scan the package surface or barcode}
    \item {Confirm Detection *}
\end{enumerate}
Each step marked with a * will be recorded as a timestamp for the evaluation of the results.\newline
The first step is self-explanatory. The application starts and with it the world tracking. The time for the timestamps is set to 0. As soon as the application is started, the user manually looks for the two markers with the camera. As soon as both markers were found, detected and verified according to section \ref{subsec:concept-registration-markers}, the time is logged. This time can be used in order to evaluate hypothesis 3. derived in section \ref{subsec:evaluation-goals} by comparing the times of different lighting conditions. Furthermore, it can be used to correctly evaluate the second detection step by setting the time it starts for every iteration. \newline
Next, the target package is selected randomly by the application and the estimated position is shown in the \gls{ar} world. The user is notified with a sound and now looks for the package. As soon as the user is confident to have found the target package by the location only, he taps the screen to log the time it would have taken him without the identification step to find the package. This is done in order to answer the question whether hypotheses 4. and 5. are correct or not, that investigate the implications of the identification step on the viablility and effectiveness of the whole concept. \newline
The user then proceeds to the identification step and tries to verify his decision by either `scanning' the barcode or using the application to identify the surface via the rectangle detection. Ususally the rectangle detection will either detect the package after a short amount of time or `scanning' the barcode is necessary. As soon as on of the above happen, the time is taken again. This time can be used to validate hypotheses 1., 2. and 3. of the previous section. This time is the most crucial part of the evaluation since it has the largest effect on whether the application is helpful in saving time or not. The user now checks whether he and/or the application has identified the correct package. This is achieved by comparing the package number which is shown on the screen with the one written (in very small font size) on the package. This data can be used to validate hypothesis 4. The application will also tell the user which identification method is used in order to draw conclusions on when the rectangle detector works better and when the barcode detector is needed.\newline
\subsection{Data recorded}
\label{subsec:evaluation-data}
This process results in the following data being recorded and availible for evaluation:
\begin{itemize}
    \item The target package number
    \item The time it took to find the world origin
    \item The time it took the user to locate the package
    \item The time it took the application to identify the package
    \item Whether or not the user picked the correct package according to position data
    \item Whether or not the application identified the correct package
    \item The distance from smartphone to package at the point of identification
    \item The detector used (rectangle or barcode)
\end{itemize}
In addition to the data mentioned in the previous section, the target package number is recorded in order to identfy packages the detection can identify very well or has difficulties identifying. This way, an analysis of the factors influencing the detection is possible. Furthermore, the distance from the smartphone to the package at the point of identification is logged. This gives an indication how far the user has to be away from the package for the different identification methods to work.
\subsection{Variations}
\label{subsec:evaluation-variations}
There are a few factors that can and will be varied throughout the evaluation iterations in order to be able to validate the hypotheses derived in \ref{subsec:evaluation-goals}:
\begin{itemize}
    \item The total amount of packages
    \item The lighting conditions (daylight or dim, artificial lighting)
\end{itemize}
These variations will be noted before each test run. This is done in order to determine which factors have the most impact on detection accuracy and speed. The total amount of packages will be varied in order to show a difference in time gain between using the application and searching for packages manually. As described in hypothesis 2, it is to be expected, that the time gain from the application is higher, the more packages total there are. The lighting conditions will be varied in order to show, that the lighting conditions do have an effect on the accuracy and speed of the detection. As stated in hypothesis 3., since detection is done in an optical way it is to be expected that the application works better in a good, well lit environment compared to a dim environment. A comparison of the two different lighting conditions can be seen in \ref{fig:evaluation-lighting}. The bright lighting conditions in daylight are on the left whereas the dim, artificial lighting at night can be seen on the right side of the figure.

\begin{figure}[!ht]
    \centering{
    {\includegraphics[width=\textwidth]{images/05-01-evaluation-lighting.png}}
    \caption{Different Lighting Conditions: Bright and dim}
    \label{fig:evaluation-lighting}
    }
\end{figure}