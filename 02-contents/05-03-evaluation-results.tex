\section{Evaluation Results and Discussion}
\label{sec:evaluation-results}
This section will present and discuss the results from the evaluation described in section \ref{sec:evaluation-setup} and \ref{sec:live-test}. At first, in \ref{sec:evaluation-results}, the data that was recorded during the evaluation is presented. Furthermore, in this section, the implications of the resulting data are discussed. Finally, in section \ref{subsec:evaluation-conclusion}, a conclusion about the lessons learned in the evaluation is drawn.

\subsection{Results and Discussion}
\label{subsec:evaluation-results}
In this section, the evaluation results are presented and discussed. All data recorded can be found in appendix \ref{sec:tables-evaluation-data}. In order to do this, the hypotheses presented in section \ref{subsec:evaluation-goals} are either proven or disproven one by one by looking at the data gathered. \newline
At first, though, some general performance indicators are calculated. The total number of attempted detections was $d_{t} = 140$. Out of those 140 detections, the number of correct detections was $d_{c} = 135$. The number of returned detections, no matter whether they were false or correct, was $\d_{r} = 140$. With the help of this data, a first overall performance indicator can be calculated.
$$ d_{t}: \textrm{total number of expected detections} $$
$$ d_{c}: \textrm{number of correct detections} $$
$$ d_{r}: \textrm{number of actual returned detections, no matter whether correct or false} $$
$$recall = \frac{d_{c}}{d_{t}} \approx 0.96$$
$$precision = \frac{d_{c}}{d_{r}} \approx 0.96$$
With a recall of 0.96, 96\% of all possible packages were detected correctly. The precision of 0.96 indicates that almost no false packages were detected, most of the detection that were made were also correct. An analysis of some of the false detections will be performed in at the end of this section in paragraph \ref{subsubsec:evaluation-analysis}. But these numbers alone are no indicator whether the application is viable or not. To determine this, a look at the times it took for detection is essential. Because no matter how accurate the detection is, when there is no time to be saved, a use of this concept in the real world is very unlikely.
\subsubsection{Hypotheses 1. and 2.}
\label{subsubsec:evaluation-hyp12}
Now that the general performance of the application is determined, hypotheses 1. and 2. are looked at. As a reminder, the hypotheses stated, that
\begin{itemize}
  \item The use of the application decreases the average time it takes to find a package compared to searching
  \item the more packages total there are, the larger the time gap between using and not using the application gets
\end{itemize}
 To get an indication of whether the detection actually is faster than just searching for the package, the average detection times are compared to the times it took on average to find a package while searching for it. This is shown in \ref{fig:evaluation-avg-time-numbers}. As already expected, as described in section \ref{subsec:evaluation-goals} in hypothesis 2., the manual search is faster when there are very few packages but the time it takes to search for a package gets significantly higher the more packages there are. While this is also true for the use of the application, the time does not get higher as fast as it does for the manual search.



\begin{figure}[htbp]
    \centering
    \begin{tikzpicture}
       \begin{axis}[
           ybar,
           enlargelimits=0.15,
           legend style={at={(0.5,-0.15)},
             anchor=north,legend columns=-1},
           ylabel={time [s]},
           symbolic x coords={10, 20},
           xtick=data,
           nodes near coords,
           nodes near coords align={vertical},
           ]
       \addplot coordinates {(10,10.335) (20,19.87)};
       \addplot coordinates {(10,10.78) (20,10.96)};
       \legend{manual search, use of app}
       \end{axis}
   \end{tikzpicture}
   \caption{Time it took to find the package using the app and searching manually for different package totals}
   \label{fig:evaluation-avg-time-numbers}
\end{figure}

When performing a simple linear regression on the data points availible, the resulting regression lines can be seen in \ref{fig:evaluation-time-numbers-regression}. The choice for linear regression was made because essentially, both cases are a linear search. That means, the possible choices are looked at one by one and compared to the item searched. As expected, the search time when searching manually increases significantly the more packages there are to search through. Using the application, the search time does increase only very slightly. This is because it is immediately clear where to search and the narrowed down area is always roughly the same size. As already stated above, finding the markers always takes a certain amount of time that does not differ much. And the time for the following identification process in most cases also lies within a rather narrow range. As can be seen in the figure, the regression lines intersect at a total package amount of 10.57. Using the application is faster when there are more than 10 packages total compared to the manual search. Below that threshold, the manual search is faster. Of course the detection time when searching manually can be decreased by remembering some positions. But this also gets more and more difficult the more packages there are. In conclusion, hypotheses 1. and 2. are proven, presuming there are more than 10 packages in total. \newline

\begin{figure}[htbp]
    \centering
    \begin{tikzpicture}
      \begin{axis}[domain  = 0:40,
        samples = 2,% <- nur 100 statt 400
        xmin    = 5,
        xmax    = 40,
        ymin    = 0,
        ymax    = 35,
        ytick   = {5,10,15,20,25,30},
        xtick   = {5,20,30},
        xlabel  = {number of packages [amount]},
        ylabel  = {detection time [s]},
        extra x ticks = {10.57},
        xlabel near ticks,
        ylabel near ticks,
        set layers,
        legend style={at={(1,0)},anchor=south east}
       ]
        \addplot+[mark=none] {0.954*x + 0.8};
        \addplot+[mark=none] {0.0181*x + 10.6}; 
        \draw[dashed,thin] (axis cs: 10.57, 0 )-- (axis cs: 10.57, 35);
        \addplot[color = blue, fill = blue, mark = *, only marks]
         coordinates {
          ( 10, 10.33 )
          ( 20, 19.87 )
        };
        \addplot[color = red, fill = red, mark = *, only marks]
         coordinates {
          ( 10, 10.775 )
          ( 20, 10.96 )
        };
      \legend{manual search, use of app}
      \end{axis}
    \end{tikzpicture}
  \caption{Regression line for the time it takes to find a package using the app vs searching manually depending on the total number of packages}
  \label{fig:evaluation-time-numbers-regression}
\end{figure}

\subsubsection{Hypothesis 3.}
\label{subsubsec:evaluation-hyp3}
The next hypothesis, that was derived in section \ref{subsec:evaluation-goals} was
\begin{itemize}
  \item Since this is an optical system, it is susceptible to lighting conditions
\end{itemize}
To prove this, the detection times on average under different lighting conditions are looked at. \ref{fig:evaluation-avg-time-lighting} shows the average detection times it took to find the world origin and then the average time it took to identify a package under two different lighting conditions. On time under bright daylight and one time using rather dim, artificial lighting. The differences between the two lighting conditions are further described in the section \ref{sec:live-test} of the evaluation. \newline
The data shows, that the time it took to find the world origin was roughly the same. It took around \SI{3}{\second} on average in both lighting conditions to find the markers for setting the world origin. This might be due to the fact that the application knows exactly how the marker looks and can perform white balance and color corrections on the picture to get a more accurate and faster detection. Furthermore, as the picture of the comparison of the different lighting conditions in \ref{sec:live-test} shows, at the front, where the markers are located, the light is rather bright in comparison to the back of the virtual cargo space. Therefore the finding of the markers in these conditions might not be affected as much. On the other hand, the data indicates that the detection of the packages itself suffered a fair amount by worse lighting conditions. On average, with good lighting it took only \SI{11.2}{\second} to detect the target package. In dim lighting, the detection time was \SI{15.26}{\second} on average, resulting in an increase of a little over $\frac{1}{3}$. By investigating this further, the reason for this can be found by looking at the detection methods. \ref{tbl:evaluation-dim-bright} lists different statistics for each the different lighting conditions in regard of the detection mehtod used for the package detection. In bright lighting, the pacakge was detected in 66\% of cases by the rectangle detection and only 34\% of the times by the barcode detection. In dim lighting, the numbers shift in favor of the barcode detection. In that case, only 40\% of packages were detected by the rectangle detection, whereas 60\% of packages were detected by the barcode detection. This data indicates, that the barcode detection somehow takes more time than the rectangle detection. The reason for this is, that barcode detection can be done only from a closer distance and sometimes requires the user to pick up the package in order to scan the barcode. Therefore, the time difference makes sense. This is also backed up by the data that shows that the average distance the smartphone was from the package at the time of detection. For the barcode detection, the distance was $\approx$ \SI{0.48}{\metre} on average whereas the average distance for the rectangle detection was $\approx$ \SI{0.92}{\metre}. This means, the user has to approach the package much further in order for the barcode detection to work compared to the rectangle detection. And getting closer to the package, of course, takes time.

\begin{table}[ht]
  \centering 
  \begin{tabular}{c c c c} 
  \toprule
   & Dim Lighting & Bright Lighting & Average \\ [0.5ex]
  \hline
  Detection Time & \SI{15.26}{\second} & \SI{11.26}{\second} & \SI{12.59}{\second} \\
  Rectangle Detection & 40\% & 66\%  & 58.57\% \\
  Barcode Detection & 60\% & 34\% & 41,43\% \\
  No Detection & 0\% & 0\% & 0\% \\
  Time between pick and identification & $\approx$6.379 s & $\approx$2.5 s & $\approx$3.6 s \\
   \bottomrule
  \end{tabular}
  \caption{Comparison between lighting conditions}
  \label{tbl:evaluation-dim-bright}
\end{table}

In conclusion, this paragraph proves hypothesis 3. that argues that since detection is an optical system, it is affected by lighting conditions. In dim lighting, especially the rectangle detecion does not work as well. This is likely because, as explained in the technical background section \ref{subsubsec:background-rectangle-detection}, rectangle detection depends on edge detection. And since contrast is lower in dim lighting, edges and thus rectangles are not easy to detect. This leads to the barcode detection being necessary, which is slower because the camera must be closer to the package. Furthermore, the barcode detection itself also suffers from the worse lighting conditions.

\begin{figure}[htbp]
    \centering
    \begin{tikzpicture}
       \begin{axis}[
           ybar,
           enlargelimits=0.15,
           legend style={at={(0.5,-0.15)},
             anchor=north,legend columns=-1},
           ylabel={time [s]},
           symbolic x coords={marker, package, total},
           xtick=data,
           nodes near coords,
           nodes near coords align={vertical},
           ]
       \addplot coordinates {(marker,2.93) (package,12.33) (total,15.26)};
       \addplot coordinates {(marker,2.90) (package,8.36) (total,11.26)};
       \legend{dim, bright}
       \end{axis}
   \end{tikzpicture}
   \caption{Time it took to find the world origin markers, the package and the time detection took total in different lighting conditions}
   \label{fig:evaluation-avg-time-lighting}
\end{figure}

\subsubsection{Hypothesis 4.}
\label{subsubsec:evaluation-hyp4}
The next hypothesis stated in section \ref{subsec:evaluation-goals} was
\begin{itemize}
  \item The user will pick the correct package most of the time only by looking at the estimated position after finding the world origin
\end{itemize}
In order to prove this hypothesis, the data collected about the time when the user hand picked the package and whether his decision was in fact the target package is needed.

\begin{table}[h]
  \centering
  \begin{threeparttable}
    \makebox[\linewidth]{%
      \renewcommand{\TPTminimum}{\linewidth}
      \begin{tabular}{crr@{ }l}
        \toprule
        Descriptor & \mc{3}{c}{Value} \\
        \cmidrule[0.4pt](r{0.25em}){1-1} \cmidrule[0.4pt](lr{0.25em}){2-4}
        Hand Picking Wrong Package & $=$ & 2.1 & $\%$ \\
        Detection before Hand Picking & $=$ & 5.7 & $\%$ \\
        Hand Picking Correct Package & $=$ & 97.9 & $\%$ \\
        Correction Rate & $=$ & 100 & $\%$ \\
        \bottomrule
      \end{tabular}
    }
  \end{threeparttable}
  \caption[Comparison of Hand Picking and Detection]{Comparison of Hand Picking and Detection}
  \label{tbl:evaluation-comparison-handpicking-detection}
\end{table}


The data shows that in only 2.1\% of all cases, the package picked by the user was the wrong one. In 5.7\% of all cases, the user did not hand pick a package at all because detection on the phone was faster than the user could decide. In conclusion, in 97.9\% of all cases, the user did in fact pick the correct package. The identification step corrected the user every time in his decision in the cases he picked wrong. In those cases the detection went wrong, the user was able to manually correct the error by looking at the visual clues and then comparing the package label. Additionally, as can be seen in \ref{tbl:evaluation-dim-bright}, the data shows that especially in good lighting conditions, the difference in time between the user hand picking the package and the application detecting it was only roughly \SI{2.5}{\second} on average. The identification step is not needed in most cases but provides additional security in picking the correct package by validating the user's decision. And in the few cases the user picked the wrong package, the identification step corrected this decision every time. Furthermore, this step takes little to no time, making it very helpful in the whole process. \newline In conclusion, this proves the hypothesis that the user will hand pick correctly most of the time, but not all the time.
\subsubsection{Hypothesis 5.}
\label{subsubsec:evaluation-hyp5}
The last hypothesis stated in section \ref{subsec:evaluation-goals} was
\begin{itemize}
  \item The actual detection will sometimes take more time than searching the target area manually
\end{itemize}
This hypothesis can be analyzed by looking at the times it takes between the user hand picking a package and the application detecting it. These times can be seen in \ref{tbl:evaluation-dim-bright} where the different lighting conditions were compared. In good lighting conditions, while the hypothesis is true for most cases, as already stated the margin it takes longer is negligible. On average, it is only \SI{2.5}{\second} more for the identification after the manual picking of the target package. When lighting conditions are bad, though, the difference is as high as \SI{6.3}{\second} on average. In these cases, it would most likely be fastest to perform the first step of the whole application concept and finding the world origin and thus, the area where the package is expected. Only then, proceed the process by not using the application for identification but rather identifying the package manually by looking at the name and address on the package label. Since lighting conditions can be easily controlled in the real world by simply mounting lights into the cargo space, the conditions in bright light bring more weight to the analysis. In conclusion, hypothesis 5. is true for almost most cases. But the average margin by that the actual detection is slower is negligible considering it can serve as a validation step for wrongly chosen packages by the user.
\subsubsection{Further Analysis}
\label{subsubsec:evaluation-analysis}
As already stated above, the precision value of 1 of the detection, despite being very good, implies that there were some false detections during the evaluation. To find out, why the detection detected a wrong package, a look at the false positives is essential. An example for such an occurence can be seen in the appendix in table \ref{tab:measurements3}. This is an evaluation iteration where two of these false detections are visible is listed. Both times, the package in question was package number 12 which is already suspicious. Furthermore, both times, the rectangle detection found the package rather fast. By looking at the packages in question which are depicted in \ref{fig:evaluation-false-positives}, the reason becomes apparent. The target package was package number 12, which is indicated by the blue `12' in the picture. The package that was falsely detected was package number 13, indicated by the red `13'. As can be seen in the picture, the two top surfaces of the packages are very similar \textendash{} in size as well as in colors. The measurements of the surfaces of the two packages are as follows.
$$ \textrm{package 12: } 30.0 \times 24.5 \textrm{ \SI{}{\centi\metre}}$$
$$ \textrm{package 13: } 30.5 \times 23.5 \textrm{ \SI{}{\centi\metre}}$$ 
As described in the concept and implementation chapter \ref{chap:concept}, the validation is done by comparing the measurements of the packages with the use of a threshold $\gamma$. The value chosen for $\gamma$ was $\gamma = 0.25$ which corresponds to only \SI{2.5}{\centi\metre}. The differences between the packages are only \SI{0.5}{\centi\metre} and \SI{1}{\centi\metre} respectively. This falls well in the range of the threshold and a mix-up of the two when looking at size makes sense. Furthermore, the two packages also have the same dominant colors: brown, white and black. This makes the last validation step, validating by color, pointless. Concluding, as already expected, the concept has the weakness of mixing up very similar packages. As already discussed when working out the concept in chapter \ref{chap:concept}, there are ways to prevent this. For example lowering the distance threshold $\tau$, that sets the distance the detection can be from the expected position, could prevent something like this to happen. But the downside to this is that possibly misplaced packages due to rough braking or cornering could not be detected anymore in case they are misplaced by more than the threshold. It should be noted, though, that even when the detection detects the wrong package, the marker for the expected package was at another place than the marker for the detected package. This became apparent the moment the detection was made and the user realized that something could be wrong with the detection. In this case, it is easy to validate the choice manually by just looking at the name and address of the receiver of the package printed on the label or by scanning the barcode.
\begin{figure}[!ht]
  \centering{
  {\includegraphics[width=0.8\textwidth]{images/05-03-evaluation-false-positives.jpg}}
  \caption{Example of a False Detection}
  \label{fig:evaluation-false-positives}
  }
\end{figure}

\subsection{Evaluation Conclusion}
\label{subsec:evaluation-conclusion}
Looking at the analysis performed in the previous sections, most of the five hypotheses proved to be correct, although in some cases with a condition. To summarize, the hypotheses and their conditions are listed here again.
\begin{enumerate}
  \item \textbf{Hypothesis 1. } states that the average time it takes to find a package decreases by using the app. This is true in case there are more than 10 packages total.
  \item \textbf{Hypothesis 2. } states that the more packages total there are, the larger the time gap gets. This is also true.
  \item \textbf{Hypothesis 3. } states since detection is done in an optical way, worse lighting conditions lead to worse results. This is also true.
  \item \textbf{Hypothesis 4. } the user almost always hand picks the correct package after detection step one. This is true.
  \item \textbf{Hypothesis 5. } states that actual detection sometimes takes more time than hand picking the package after detection step one. This is also true, although by a margin of only $\approx$ \SI{2.5}{\second}.
\end{enumerate}
Concluding, the concept worked very well, especially in good lighting conditions. The first detection step where the world origin is found with the help of markers and the package position is estimated based on that, performed very good. The actual identification suffered a fair deal of efficiency when lighting is worse, which can easily be couteracted by mounting lights in the cargo space. Furthermore, the identification takes longer than just hand picking the actual package after the first step. Because of the fact that the difference between picking and identification is very low, it is still a good validation step. The misdetections that were described in section \ref{subsubsec:evaluation-analysis} are difficult to solve in a technical way. But since this is an interactive system and the user can easily detect them using the visual clues displayed, they are not by any means an impairing factor for the concept.