\cleardoublepage
\section{Implementation}
\label{sec:implementation}
This section describes the details of the implementation of the concept introduced in section \ref{sec:concept}. These parts are split into the same subcategories as in the concept section. But first, a few general implementation choices are explained in \ref{subsec:impl-general}. The following section \ref{subsec:impl-anchor-placement}, the implementation details about the \gls{ar} world in general and about finding the world origin with the help of markers is explained. Section \ref{subsec:impl-anchor-placement} talks about the way augmented content is placed into the real world. And finally section \ref{subsec:impl-package-identification} contains details about the package identification process using rectangle detection and barcodes.\newline
The implementation is done for iOS 12 in Swift 4, since the hardware for testing will be an iPhone Xs. iOS 12 already provides several frameworks that help the developer create \gls{ar} experiences and perform computer vision tasks. For more details on the framework, refer to chapter \ref{chap:technical-background}.
\subsection{General Implemetation Details}
\label{subsec:impl-general}
The implementation is done for iOS 12 in Swift 4, since the hardware for testing will be an iPhone Xs. iOS 12 already provides several frameworks that help the developer create \gls{ar} experiences and perform computer vision tasks. For more details on the essential frameworks used, refer to chapter \ref{chap:technical-background}.
For iOS app, the developer has to make the choice of a framework to render the content of the display. In this case, the decision is simple. According to \cite{arkit-arconfiguration.2019}, the easiest way to implement an app with the use of ARKit that is able to blend virtual content into the real world is by using an \textit{ARSCNView}, which is part of the \textit{SceneKit} framework. The advantages of this combination are that the ARSCNView will automatically display the video feed of the camera as a background of the scene. Furthermore, the scene's coordinate system is initialized in a way that it corresponds to real world coordinates. This way, in order to display virtual content relative to real world positions, no complicated calculations have to be made. It is simply possible to display a visual clue \SI{1}{\metre} to the right to an anchor point in the real world by adding `1' to the x-coordinate of said anchor. And this is exactly what will be done many times in the concept described before in section \ref{sec:concept}.
\cleardoublepage
In order to be able to detect the packages, the information about them has to be stored somehow. To do this, a packageData class was created. The class contains all information necessary for the detection, which is listed below.
\begin{itemize}
    \item \textbf{number:} the package's identification number
    \item \textbf{measurements:} the package's length, width and height in meters
    \item \textbf{position:} the package's position in the cargo space as a 3 dimensional vector
    \item \textbf{barcodeValue:} the payload value of the package's barcode
    \item \textbf{colors:} the package's colors on the surfaces of the package
\end{itemize}
The number is used in order to be able to quickly identify the package manually to validate the detection in the evaluation step. The measurements are needed for the rectangle detection to be able to compare the detected rectangle's sizes to the data stored here. The position is also used in the detection in order to know where the package is expected and in order to validate whether it is the correct package. The barcodeValue is the payload of the barcode that is fixed on the package's label. In case the barcode detection detects a barcode, it is decoded and the value is compared to the value stored here. The colors are also needed for the rectangle detection for the last validation step.\newline
This class can be seen in appendix \ref{lst:packageData} and will later be used to store the information of each package that is part of the evaluation. The evaluation process itself is described in detail in chapter \ref{chap:evaluation}.
\subsection{Registration and Orientation with Markers}
\label{subsec:impl-registration-markers}
When implementing an \gls{ar} application for iOS with the help of ARKit, the first decision to be made is for an ARKit ARConfiguration. The different possible configurations are explained in \ref{subsubsec:background-arkit-world-tracking}. In short, only an \textit{ARImageTrackingConfiguration} or an \textit{ARWorldTrackingConfiguration} are possible choices for this concept. The ARImageTrackingConfiguration can only track images and not the world around it. This is a problem in case the markers used for setting the world origin are not in the field of vision of the camera anymore. Hence, an ARWorldTrackingConfiguration is needed. This configuration ensures that the smartphone tracks the world around it using it's camera an motion sensors and allows for virtual content to be placed at real world positions, even if they are not in the field of view. Furthermore, the hit test functionality is also needed when trying to identify a package in the second identification loop of the concept in mind. It also provides the possibility for Image Tracking similar to the ARImageTrackingConfiguration, but with a lower sample rate. This will be needed first, when the markers are to be detected in the real world.\newline
In order to be able to find the cargo space and set the world origin, the application now has to detect the markers that mark the cargo space. At first, markers have to be chosen. As already described in \ref{subsec:concept-registration-markers}, they can be arbitrary images but it is best if they have high contrasts. There are several marker generators availible that can generate random markers optimized for detection by \gls{ar} applications. Two examples were already presented in the technical background section \ref{subsubsec:ar-methods-registration}. The decision for this thesis was made in favor of the markers created by the generator by \citet{arkit-marker-generator-codepen.2019}. The evaluation and reasoning why are described in \ref{subsec:implementation-ar-markers}. The markers used look like the marker depicted in \ref{fig:implementation-armarker}.

\begin{figure}[!ht]
    \centering{
    {\includegraphics[width=0.5\textwidth]{images/04-03-marker1.png}}
    \caption{AR marker type used, generator by \citet{arkit-marker-generator-codepen.2019}}
    \label{fig:implementation-armarker}
    }
\end{figure}

In order to be decteted, the markers at first have to be stored in an assets folder and the real world measurements have to be entered. In this case, the markers were printed on an A4 paper which resulted in measurements of \SI{21}{\centi\metre} $\times$  \SI{21}{\centi\metre}. When this is done, the ARWorldTrackingConfiguration comes with the possibility to provide the configuration with these images and it will automatically find and track them in the real world. As soon as one of the images is found, a delegate (esentially a callback) function is called. This enables the developer to react to certain events. As soon as the delegate is called that a reference image has been found, the validation process descibed in \ref{subsec:concept-registration-markers} is started. This essentially boils down to waiting until both markers have been found and then comparing their positions and orientations. For each reference image, ARKit creates a so called \textit{ARAnchor}. An ARAnchor consists of a real-world position and orientation and is used to display objects relative to them or reference points in the real world. So to compare the detected orientation of both markers, the only thing to do is compare the orientation of both respective ARAnchors. In case they are below the set threshold, their positions are taken, substracted and the world origin is set according to the concept. In case they exceed the threshold all anchors are removed from the scene, making them availible for detection again. The next detection will be more accurate though, because world tracking will have collected more data and has refined the model of the real world in the meantime.
\cleardoublepage
\subsection{Augmenting the World}
\label{subsec:impl-anchor-placement}
As soon as the world origin is set, the package position of the target package can be displayed. The position saved in the package data is relative to the world origin. Because of the work done in the previous section, by setting the world origin and the orientation according to the cargo space, all there is to do in this step is to create an ARAnchor at the target position and display a virtual clue there. Because of this and the fact that ARKit measures distances in meters, the unit that is also used in the package data, there are no additional calculations necessary. The state of the application after setting the new world origin and loading the data of a package can be seen in the screenshot in \ref{fig:impl-world-origin}. The world origin is displayed with the 3 axis in between the markers. The estimated position of the target package (the yellow one) is displayed as `packageToFind' in the \gls{ar} world with the help of a visual clue. This way, the user knows in which direction to go for the identification. \newline

\begin{figure}[!ht]
    \centering{
    {\includegraphics[width=\textwidth]{images/04-03-application-world-origin.png}}
    \caption{World Origin and Package Position in the Application}
    \label{fig:impl-world-origin}
    }
\end{figure}

To start the package identification process described in the next section \ref{subsec:impl-package-identification}, the user has to be within a certain distance to the target package. In order to to this, the current location of the smartphone and the location of the target package have to be compared regularly. ARKit provides a delegate function every time a new frame is rendered (in this case, 60 times a second). This delegate contains an ARFrame, in which the camera's world coordinates are stored. The only thing left to do is to calculate the distance between the two points according to section \ref{subsec:concept-anchor-placement}. The same function compares the distance to the threshold and sets a flag to start the package identification as soon as the value falls below the threshold. The value for this threshold $T$ that is used in the implemetation is $T = 1.5$, which corresponds to 1.5 meters distance.

\cleardoublepage
\subsection{Package Identification}
\label{subsec:impl-package-identification}
As soon as the package identification starts, every time the delegate function for a new ARFrame is called, the search for a rectangle and the search for a barcode is triggered. To improve performance and reduce battery drain, this is not done every frame, but every 5 frames. At first, performance wise it was planned to run the barcode detection only in case rectangle detection fails. But as explained in \ref{subsubsec:implementation-discarded-backup}, this is not necessary. Thus, in this implementation,b oth detections run in different, parallel threads. The first positive detection from either of them serves as the detection result and detection ends.
\subsubsection{Rectangle Detection}
The rectangle detection process in implemented in a \textit{findRectangle()} function according to the concept described in \ref{subsubsec:concept-rectangle-detection}. But there are a few challenges on the way. At first, the current capturedImage property of the ARFrame is given to a rectangle detector. The rectangle detector used in this case is the Apple Vision Rectangle Detection. For more details about the Vision framework, refer to section \ref{subsec:background-vision-coreml}. The choice for this rectangle detector was simply becaus it works very well together with ARKit and Apple's other frameworks. It also detects rectangles reasonably well and the options for improving performance provided are fitting to the problem at hand. Thus, the need for a third party framework seemed unnecessary. Essentially, the rectangle detector analyzes the image given to it and detects real-world rectangles. But as already stated, there is room for optimization. First of all, the minimum confidence of the detection can be tinkered with. A higher minium confidence leads to fewer, but more accurate detections whereas a lower minimum confidence leads to more, but maybe less accurate detections. Since usually there are many rectangles in the picture, because there are many packages in the cargo space, a rather high confidence threshold proved to be working better. The current implementation uses a minimum confidence level of 70\%. Another possible and very promising optimization option is the fact that a minimum and maximum aspect ratio of the target rectangle can be set. This is very helpful in this case, because the aspect ratio of the target rectangle is exactly known, since all measurements of the target packages are stored in the package data. So for each package, the detector is configured in a way that it specifically detects rectangles that have the target aspect ratio which is calculated using the data availible. Of course because of possible detection inaccuracies, a threshold is applied here as well. An example for a detected rectangle is shown in the screenshot in \ref{fig:impl-rectangle-detection}. In this case, the rectangle's shape was visualized using a green outline. The detected rectangle fits almost exactly over the package's surface. When such a rectangle is detected, the next step can be performed.

\begin{figure}[!ht]
    \centering{
    {\includegraphics[width=0.6\textwidth]{images/04-03-application-rectangle-detection.png}}
    \caption{Visualization of a detected rectangle}
    \label{fig:impl-rectangle-detection}
    }
\end{figure}

The detection done by the rectangle detector is performed on a 2D image. Thus, the coordinates of where in the image the rectangle is detected are given in 2D coordinate space. These coordinates are normalized vectors in the coordinate space of the input image. To perform hit tests as referred to in \ref{subsubsec:background-arkit-world-tracking} and to do visualizations of the rectangle in like in the screenshot, the coordinates are needed in display coordinate space, though. The conversion of this is rather complicated. The image given to the detector that the ARFrame provides has a resolution of 1920x1440 pixels, which results in an aspect ratio of 4:3. This is the default resolution for cameras. The viewport, e.g. the space on the screen the ARScene is rendered in, has an aspect ratio of 16:9. Hence, the image shown on the screen is cropped. This has to be accounted for when converting from the normalized vector image coordinates to screen coordinates. What further complicates the calculation is that the camera coordinates change when flipping the smartphone from portrait to landscape mode. The function in Listing \ref{lst:convertfromcamera} is used for the conversion.
\begin{minipage}[t]{\textwidth}
\begin{code}
    \begin{lstlisting}[firstnumber=11,language=swift]
//Convert Detected Rectangle Coordinates into Screen coordinates
    func convertFromCamera(_ point: CGPoint) -> CGPoint {
        let orientation = UIApplication.shared.statusBarOrientation
            
        let height = sceneView.bounds.height
        let width = sceneView.bounds.height * 3 / 4
        let offsetX = (self.sceneView.bounds.width - width) / 2
            
        switch orientation {
            case .portrait, .unknown:
                return CGPoint(x: point.y * width + offsetX, y: point.x * height)
            case .landscapeLeft:
                return CGPoint(x: (1 - point.x) * width, y: point.y * height)
            case .landscapeRight:
                return CGPoint(x: point.x * width, y: (1 - point.y) * height)
            case .portraitUpsideDown:
                return CGPoint(x: (1 - point.y) * width, y: (1 - point.x) * height)
        }
    }
    \end{lstlisting}
    \caption[convertFromCamera function]{\texttt{convertFromCamera} function. This shows how to convert from VNRectangleObservationResult coordinates to screen coordinates.}
    \label{lst:convertfromcamera}
  \end{code}
\end{minipage}
At first, the current smartphone orientation is read from the UIApplication. Then, the current height from the scene is taken. The width is then calculated using the height and the aspect ratio of the source image. This results in a frame that is actually larger than the screen. This overlapping area represents the cropped part of the image due to the smaller viewport. The coordinates then have to be shifted by the cropped margin devided by two to fit the screen coordinates. Next, depending on the orientation of the smartphone, the coordinates are calculated taking into account the swap of x and y coordinates due to the different orientation of the camera. \newline
As soon as all corners of the rectangle have been converted to screen coordinates, it can be rendered on the screen. Furthermore, and more importantly, the hit test can be performed. The hit test, as alreary stated, finally returns the coordinates of the rectangle in world coordinate space. Now, the next check can be performed: Is the detected rectangle located near the target package's location data? This check is simply implemented according to the algorithm described in the concept in section \ref{subsubsec:concept-rectangle-detection}. A threshold $\tau$ that proved to be working well is $\tau = 0.4$, which corresponds to a maximum distance of 40 cm.\newline
The next step is to check the sides of the detected rectangle's real world coordinates for perpendicularity. Again, this is done according to the concept. The dot product of the two vectors of the sides is calculated and then compared to a threshold. In this case, the threshold $\sigma$ chosen is $\sigma = 0.01$. \newline
Now, the final step of the detection process is implemented: The color comparison. But before any colors can be compared, the texture of the detected rectangle has to be extracted and deskewed, because in pixel coordinates the detected and confirmed rectangle might not be one due to the camera perspective as described in the concept. Fortunately, Swift already provides the developer with a function to deskew part of an image. The function \textit{perspectiveCorrected} is part of the ciImage class and only needs the image coordinates of the corners of the rectangle to deskew. Note that image coordinates are again different coordinates than the screen coordinates (and of course the world coordinates) because the image coordinates are the coordinates before the image was resized to the screen's resolution and cropped to the screen's aspect ratio, but are also not the normalized vectors the rectangle detection provides. The calculation from normalized coordinate vectors to image coordinates is rather simple, but because this function is used again at a later stage, it can be seen in \ref{lst:adjustToImageSize}. As the Listing, each coordinate is multiplied with the extent of the same dimension. The extent is the size of the source image in pixels.
\begin{minipage}[t]{\textwidth}
\begin{code}
\begin{lstlisting}[firstnumber=11,language=swift]
func adjustToImageSize(point: CGPoint, extent: CGRect) -> CGPoint {
    var normalized = CGPoint()
    normalized.x = point.x * extent.width
    normalized.y = point.y * extent.height
    return normalized
}
    \end{lstlisting}
    \caption[adjustToImageSize function]{\texttt{adjustToImageSize} function. This shows how to convert from coordinates in a normalized vector space to absolute image coordinates.}
    \label{lst:adjustToImageSize}
  \end{code}
\end{minipage}
After the perspectiveCorrected function is applied with the converted coordinates of the detected rectangle, the result is a deskewed image of the package's surface that was detected. This image can now be analyzed by it's colors.\newline
The color analysis is implemented according to the concept inf \ref{subsubsec:concept-rectangle-detection}. At first, every pixel is converted from RGB to HSV color space. How this can be done in Swift is shown in Listing \ref{lst:rgb-hsv} in the appendix. Next, every pixel is put into one of the color segments listed in \ref{table:color-segments}. The four segments with the most colors are returned as the primary colors. For a color to be considered, it has to occur at least 40 times, though. If this is not the case, there can also be less than four primary colors. If all colors match, the detection is complete and the detected package position is shown in the \gls{ar} world. To simplify the verification whether the detected surface was in fact the package's surface, a small visualization is also implemented. The surface of the package flashes for a few seconds for an easy visual confirmation by the user. This visualization can be seen in the screenshot in \ref{fig:impl-surface-visualization}, where the surface on one package is highlighted in white color. Contrary to the rectangle depicted in \ref{fig:impl-rectangle-detection}, the visualization in this case here is not calculated and rendered in 2D coordinate space but rather in real-world coordinate space. It was rendered in 3D in the \gls{ar} world with the help of the hit testing data aquired before. And as can be seen, it matches the surface of the target package. Thus, the detection was correct.

\begin{figure}[!ht]
    \centering{
    {\includegraphics[width=0.6\textwidth]{images/04-03-application-surface-visualization.png}}
    \caption{Visualization of the detected surface of the target package}
    \label{fig:impl-surface-visualization}
    }
\end{figure}

\subsubsection{Barcode Detection}
Barcode detection in iOS can also be implemented using the Vision framework. The framework provides a \textit{VNBarcodeDetectionRequest} option to automatically find and decode barcodes in images. More details about the barcode detection in Vision are described in \ref{subsec:background-vision-coreml}.\newline
To find a barcode in an image, the request is first given an array of different barcode types that should be supported. Since the set of test packages used all have shipping information and barcodes from DHL or Hermes, the types of their barcodes will be supported. Namely, these are \textit{Interleaved 2 of 5 barcodes}, sometimes with a checksum. The details about the barcodes are rather irrelevant for the implementation, the only important thing is whether they are supported by the Vision framework or not. As can be seen in the technical background section about Vision, Interleaved 2 of 5 is supported as \textit{VNBarcodeSymbology .I2of5}. Just like the rectangle detection, the barcode detection is performed every 5 frames. The detector returns an array of detected barcodes for each frame. The Vision framework provides the function to decode the barcode as well as determine it's position in image coordinates. The comparison of the payload in this case can be literal and without a threshold since the decoded barcode consists of a series of numbers. This series can simply be compared to the package data to verify whether the detected barcode belongs to the target package. Then, the positions of the detected barcode and the target package data are compared, just as with the rectangle detection. Again, the convertFromCamera function listed in listing \ref{lst:convertfromcamera} is used to convert the coordinates of the detection, and a hit test is performed. If the coordinates are closer than the threshold $\rho$, the detection is successful. A practical value for the threshold proved to be $\rho = 0.4$, which is the same threshold used in the rectangle detection. This corresponds to a distance of 40 cm. The result of the hit test is then used for the visualization of the package's position. This can be seen in the screenshot in figure \ref{fig:impl-package-found}. The position where it was originally assumed it would be is still marked as `packageToFind'. The location where the hit test for the detection was performed, is marked with `HitTestLocation'. As can be seen, the two points are fairly close to each other, so the assumed location was very accurate.

\begin{figure}[H]
    \centering{
    {\includegraphics[width=0.6\textwidth]{images/04-03-application-package-found.png}}
    \caption{Location marker of the package after it was found}
    \label{fig:impl-package-found}
    }
\end{figure}

\subsection{Choosing the AR Markers}
\label{subsec:implementation-ar-markers}
Since there is no literature to be found on which \gls{ar} markers exactly are used best with ARKit, two types of markers were tested that were generated from two different marker generators. The marker generators tested were already introduced in the technical background about \gls{ar} in section \ref{subsec:ar-methods}. Both of the generators promise to create markers that can be easily detected by \gls{ar} applications and thus be used for orientation in the \gls{ar} world. One of the generators is the marker generator by \citet{arkit-marker-generator-brosvision.2013} which creates markers that are ``optimized for use as augmented reality image targets''. A marker generated by this generator is depicted in \ref{fig:implementation-marker1}. The marker is a randomly generated image consisting of a number of black lines, colored triangles and other colored polygons. The other generator tested generates markers like the one that can be seen in \ref{fig:implementation-marker2}. The generator was published by \citet{arkit-marker-generator-codepen.2019} and is described as being designed specifically to generate markers optimized for ARKit and Vuforia. The markers in this case consist of a random arrangement of black circles and a few circles in two fixed colors mixed in between.

\begin{figure}
    \centering
    \begin{minipage}{.5\textwidth}
      \centering
      \includegraphics[width=.6\linewidth]{images/04-03-marker2}
      \captionof{figure}{AR Marker, by \citet{arkit-marker-generator-brosvision.2013}}
      \label{fig:implementation-marker1}
    \end{minipage}%
    \begin{minipage}{.5\textwidth}
      \centering
      \includegraphics[width=.6\linewidth]{images/04-03-marker1}
      \captionof{figure}{AR Marker, by \citet{arkit-marker-generator-codepen.2019}}
      \label{fig:implementation-marker2}
    \end{minipage}
\end{figure}


\begin{figure}[H]
    \centering
    \begin{tikzpicture}
       \begin{axis}[
           ybar,
           enlarge x limits=0.15,
           legend style={at={(0.5,-0.15)},
             anchor=north,legend columns=-1},
           ylabel={time [s]},
           ymin = 0,
           ymax = 8,
           symbolic x coords={$t$},
           xtick=data,
           nodes near coords,
           nodes near coords align={vertical},
           ]
       \addplot coordinates {($t$,6.0)};
       \addplot coordinates {($t$,3.6)};
       \legend{Brosvision, Stausbøl}
       \end{axis}
       \begin{axis}[
        ybar,
        enlarge x limits=0.15,
        ylabel={distance [m]},
        ymin=0,
        ymax=1.5,
        symbolic x coords={$d_{1}$, $d_{2}$},
        xtick=data,
        nodes near coords,
        nodes near coords align={vertical},
        axis y line=right
        ]
    \addplot coordinates {($d_{1}$,0.78) ($d_{2}$,0.74)};
    \addplot coordinates {($d_{1}$,1.12) ($d_{2}$,1.13)};
    \end{axis}
   \end{tikzpicture}
   \caption{Comparison between the Brosvision \cite{arkit-marker-generator-brosvision.2013} and Stausbøl \cite{arkit-marker-generator-codepen.2019} markers}
   \label{fig:implementation-marker-comparison}
\end{figure}

In order to evaluate which type of markers out of those two is better suited for the application implemented for the evaluation of this thesis, both were tested in an environment similar to the one the evaluation will take place. To have even more similar conditions, two of each of the markers were printed out and a real world test was made. For each pair of markers, the time it took to find both and the distance the smartphone was away from each of the markers at the time of detection was logged. The data of this test can be found in appendix \ref{tab:marker-comparison1} and \ref{tab:marker-comparison2} respectively.
The average values of the measurements are depicted in \ref{fig:implementation-marker-comparison}. It is easy to see, that the markers generated by the generator by \citet{arkit-marker-generator-codepen.2019} are easier to detect. The detection is $\approx$ \SI{2.4}{\second} quicker on average and the distances from where the markers can be detected are quite a bit higher. This makes the choice for the markers by \citet{arkit-marker-generator-codepen.2019} easy.


\subsection{Discarded Approaches}
\subsubsection{Neural Network for Package Identification}
One discarded idea of the concept was to use a neural network instead of a rectangle detector to detect packages. Since this was in interesting approach, an attempt to implement it was made. Unfortunately, due to compatibility reasons of the various frameworks and the lack of a pre-trained model, the approach was ultimately discarded. \newline
Since the classification has to run on mobile hardware and on a real-time video feed, a fast but still precise classification mechanism has to be used. Furthermore, it is of advantage if the classifier does not take as much memory because of the limited storage capacity on smartphones. The most common classifiers use a neural network and a single shot detector, which separates the image into multiple parts and performs detection on all of them. The most common being a YOLO classifier or a MobileNetv2 \gls{SSD}, both of which are lightweight and fast. More on neural network classifiers for object detection can be found in the technical background in section \ref{subsubsec:background-neural-networks}. \newline
The approach was to use a pre-trained neural network in .mlmodel format, that can be used by a combination of the Vision and CoreML frameworks. As explained in \ref{subsec:background-vision-coreml}, the Vision Framework allows the developer to manage many computer vision tasks without the hassle of converting images or doing calculations for transformations on his own. Furthermore, it makes use of CoreML, Apple's library for machine learning tasks, easier and more intuitive. CoreML itself comes with the huge advantage of always being able to select the best hardware availible on the device for the various machine learning tasks. Thus, it is not limited to the GPU but can also use the so called Neural Engine of the newest Apple SOCs. Furthermore, the machine learning model used in CoreML only consists of one .mlmodel file which takes an image as input and gives bounding boxes as an output. This makes it easily interchangable which makes further development or improvement easier. Unfortunately, the combination of Vision and CoreML is very new and thus there are only a few pre-trained neural networks in the correct format availible for free. It is possible though, to train a own model in the correct format using the Turi Create framework. As explained in \ref{subsubsec:background-neural-networks} this would take too much time in the context of this thesis, though. Thus, it is necessary to rely on a pre-trained model.\newline
Unfortunatly, only a pre-trained model that was trained using the COCO dataset by Microsoft in the correct format that is usable with CoreML was availible. The classes availible in this dataset are listed by \citet{coco.2014} in their release paper and do not contain the classes \textit{package} or \textit{box}. Nevertheless, a test was implemented to check whether the approach could even be viable. The neural network used by this classifier was a MobileNetv2 \gls{SSD}.
The classifier can simply be used with Vision similar to the rectangle detection. One difficulty that still remains after implementing the working machine learning model is to get the combination of all of these frameworks to work together. Despite Apple's various frameworks taking on many of the computer vision tasks, there are still problems in particular when it comes to image formats and aspect ratios. These problems were similar to the ones described in section \ref{subsec:impl-package-identification} where the rectangles detected have to be converted. The camera picture taken by the ARKit Scene that is accessible via the scene's ARFrame comes in a resolution of 1920x1440 pixels, which corresponds to a aspect ratio of 4:3. Now the machine learning classifiers used in CoreML usually take a square image, in this case 300x300 pixels. As already explained in \ref{subsubsec:background-neural-networks}, larger image sizes would require too much processing power to be processed in real time on mobile hardware. So the picture taken from ARKit needs to be resized in order to be accepted by CoreML. While this is done automatically by the Vision framework, the image processed is still distorted and that has to be accounted for later on. CoreML in return outputs the bounding boxes of the objects that were recognized as normalized vectors (relative to the input image). In order to get the bounding boxes in the correct place in the original image, these normalized vectors have to be transformed to fit the 4:3 input image again. In addition to that, the bounding boxes are returned rotated by 90 degrees and thus have to be rotated back. Now the phone's screen does not have a 4:3 aspect ratio so in order to display the camera image on the sceen, it has to be cropped and scaled which also has to be accounted for before drawing the bounding boxes. In summary, this results in multiple calculations necessary to get the correct screen coordinates for the result of the machine learning algorithm to know where on the screen each object is. This whole process is also important for the \gls{ar} world because in order to determine the 3D position of a point in the image, a hit test at the screen coordinates of said object has to be performed. This hit test checks whether the \gls{ar} world tracking (see section \ref{subsubsec:background-arkit-world-tracking}) has detected a plane or a feature point at this location and whether it can return a corresponding coordinate in 3D space for the point where the hit test is performed. \newline
Upon a few tests with objects availible in the dataset, the detector performed with satisfactory performance and in real time with up to 30 frames per second. Depending on the object and the distance, the detection can be unreliable sometimes. But due to the problem described above, another machine learning model would be needed in order to be able to detect packages anyway.
\subsubsection{Barcode Detection as Fallback}
\label{subsubsec:implementation-discarded-backup}
In the implementation described above, the barcode detection is used in parallel to the rectangle detection algorithm. At first, the concept was designed with the barcode detection as a fallback to the rectangle detection, though. This is because the rectangle detection was thought to be much faster than the barcode detector, especially since barcode detection requires the camera to be close to the barcode. This way it is possible to increase performance of the overall application. Only when rectangle detection failed, the barcode detection should be used as backup detector. When implementing this, it turned out that the smartphone can easily handle running both detectors in a parallel fashion. This is why the overhead of having to determine when the barcode detection should take over was removed. The barcode detector implicitly still serves as a backup but this is simply due to the fact that it requires the user to be closer, as shown in the evaluation in section \ref{sec:evaluation-results}. Since in a parallel scenario, the first detection `wins' and the rectangle detector is faster most of the time it mostly is used only when the rectangle detection fails.
