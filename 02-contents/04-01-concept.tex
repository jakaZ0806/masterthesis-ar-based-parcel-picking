\section{Concept}
\label{sec:concept}
In this section, the general concept of the solution to the problem described in chapter \ref{sec:problem-statement} is described.\newline
The general concept of the workflow of the unloading assistance is to first guide the user towards the supposed location of the package, i.e. the loaction where the package was when loading, and then perform an actual detection in the camera's view to find the package position.\newline
Thus, the problem can be split into two major parts: The first one is the registration in the \gls{ar} world. This means, the device has to know about it's surroundings, e.g. what objects are present, where they are located and how they are oriented. The technical background on registration in an \gls{ar} context are described in section \ref{subsubsec:ar-methods-registration}. Registration is needed in order for the application to recognize the cargo space of the vehicle in order to be able to determine the location of the packages and to enable the application to seamlessly integrate \gls{ar} content into the real world at the correct locations. It also enables the application to place virtual markers in the environment that stay at their real-world place indefinitely, even when the camera moves around.\newline
The second problem is the recognition of the package itself, because in order to be able to highlight it in the \gls{ar} world, the package has to be found in the camera feed. As described in section \ref{subsubsec:background-rectangle-detection}, for the recognition to be reliable, at first, the user has to be directed to move the smartphone near the package he wants to recognize. This happens by using prior knowledge where the package was placed when loading the vehicle. When the application has successfully recognized the cargo space, the locations of all packages can be loaded into the app and shown in the \gls{ar} world. In perfect conditions, this may already be enough to guide the user to the package, but since registration and thus the loaded package positions are not always completely accurate and because the packages may have shifted during transit, performing the recognition step is still necessary in most cases. \newline
The resulting flow chart for the application concept is given in \ref{fig:concept-state-machine}. After the application is started, an emtpy \gls{ar} scene is loaded and the world tracking is started. This means, that from this point on, the application is using it's sensors and camera to remember feature points visible through the camera that it can later recognize again. Through this, the application can orient itself in the world and understand it's surroundings. After world tracking is started, the application tries to find the cargo space by looking for predefined markers, that are fixed at certain positions known to the application. Once enough markers are found, the registration step is complete and the application can set the new \gls{ar} world origin to a specific point (e.g. at the center on the back side of the cargo space). The concept for these steps is described in detail in section \ref{subsec:concept-registration-markers}. Up from this point on, it is possible to augment the \gls{ar} world with virtual objects that can be placed relative to the new world origin. Now, the package positions can be loaded from either a database or in this case are hard coded into the application for the testing scenarios. For each package, a virtual object marker is placed in the \gls{ar} world. These markers can be made visible by the application and stay in their real world position no matter where the user moves the camera. At the marker of the desired package, an area can now be highlighted in the \gls{ar} world to guide the user towards it. The concept for these steps is further described in section \ref{subsec:concept-anchor-placement}\newline
In the next step, as the user moves the device closer to the estimated package position, the application tries to identify the package in the camera's view. It directs the user towards the expected position until the package itself is detected in the world. This is done via a rectangle detection and the comparison of visual features or via a barcode detector. The barcode detector is used in case the rectangle detector cannot detect the package. When detection is complete, the position of the virtual object marker can be updated, the package itself can be highlighted and the user can be notified that the package was found. The details for the package detection are explained in section \ref{subsec:concept-package-identification}.

\begin{figure}[!ht]
    \centering{
    \resizebox{\textwidth}{!}{\input{images/04-01-concept-state-machine_2.pdf_tex}}
    \caption{Application concept}
    \label{fig:concept-state-machine}
    }
\end{figure}

\subsection{Registration and Orientation with Markers}
\label{subsec:concept-registration-markers}
As described above, for the registration process the application has to start the \gls{ar} world tracking and then the search for the markers in the ar world in order to know the surroundings of the smartphone and where the cargo space is located.
World tracking achieves registration by using the smartphone's camera's video feed to extract feature points and then use these feature points in combination with the smartphone's motion sensors to create a model of the real world. This process is descibed further in the technical background sections \ref{subsubsec:ar-methods-registration} for general methodologies about registration and section \ref{subsubsec:background-arkit-world-tracking} for how world tracking works in Apple's ARKit which will be used in the implementation part of the thesis at hand. The choice in favor of a markerless registration method like the world tracking over a marker-based one was made, because the markers placed at the cargo space will not always be in the camera's field of vision while the application is active because the user moves the smartphone around while using it. Therefore, a solely marker-based registration method will not be accurate enough in case the user moves away from the markers. This will inevitably happen when the user enters the cargo space to locate and fetch the package. With the help of markerless \gls{ar} registration, the smartphone can still track the surroundings. Furthermore, the better the model of the surroundings gets, the more accurate registration will get. The two markers to find the cargo space will be used to be able to very quickly find the cargo space so the actual detection can start shortly after the user opens the cargo space doors. \newline
To be able to find the cargo space using markers, at first, markers have to be placed at locations that are visible instantly after opening the doors. The floor in front of the shelves or the doors themselves are good places to place the markers. Since the floor is there in every cargo space and it is very easy and fast to point the smartphone at the floor, the floor will be used to attach the markers. The markers themselves can be arbitrary 2D images. It is very helpful for fast detection though, when there are high contrasts and clear shapes in the image. Since the detection of a marker and it's position and orientation is not always accurate enough for this use case, two markers will be used. When one of them is detected, the other marker is used as a validation to ensure the application performed the localization correctly. To validate the detection, the yaw value, which is the orientation of an object on the perpendicular axis, will be compared as can be seen in figure \ref{fig:yaw-pitch-roll}. 

\begin{figure}[!ht]
    \centering{
    \resizebox{0.8\textwidth}{!}{\input{images/04-01-yaw_pitch_roll.pdf_tex}}
    \caption{Yaw, pitch and roll values in 3D coordinate space}
    \label{fig:yaw-pitch-roll}
    }
\end{figure}

The markers are placed on the floor of the cargo space which is a horizontal plane. In figure \ref{fig:yaw-pitch-roll}, this corrensponds to the plane the x and z axis stretch out. In this case, as shown in the figure, the roll and pitch values are fixed and the yaw value is the only value that differs and thus could be detected falsely. To determine whether the detection of the marker position and orientation was accurate, the difference $\Delta yaw$ between the yaw value of marker 1 $yaw_{1}$ and the yaw value of marker 2 $yaw_{2}$ is calculated:
$$ yaw_{1}: \textrm{yaw value of marker 1} $$
$$ yaw_{2}: \textrm{yaw value of marker 2} $$
$$ \Delta yaw = | yaw_{1} - yaw_{2} | $$
The resulting difference $\Delta yaw$ is then compared to a threshold $\tau$:
$$ \Delta yaw < \tau : \textrm{detection was accurate} $$
$$ \Delta yaw \geq \tau : \textrm{detection was not accurate} $$
In case the detection was not accurate, both detected markers are reset and detection starts from scratch. In case the detection was accurate, the new world origin $pos_{World}$ and the new orientation of the axis $yaw_{World}$ are set at the bottom between both markers according to \ref{fig:concept-world-origin} using the positions $pos_{1}$ and $pos_{2}$, orientations $y_{1}$ and $y_{2}$ of both markers. In addition, since the positions are always measured in the middle of a marker, in order to get the bottom size, half of the extent in z-direction of the marker $z_{marker}$ has to be substracted from the world origin's new position.

\begin{figure}[!ht]
    \centering{
    \resizebox{0.8\textwidth}{!}{\input{images/04-01-world-origin.pdf_tex}}
    \caption{Calculate the new world origin}
    \label{fig:concept-world-origin}
    }
\end{figure}


This leads to the following values:
\begin{align*}
     pos_{1} =
     \begin{pmatrix}
        \vec{x_{1}}  \\
        \vec{y_{1}}  \\
        \vec{z_{1}}
    \end{pmatrix} 
    : \textrm{real world position of marker 1}
\end{align*}
\begin{align*}
    pos_{2} =
    \begin{pmatrix}
        \vec{x_{2}}  \\
        \vec{y_{2}} \\
        \vec{z_{2}}
   \end{pmatrix} 
   : \textrm{real world position of marker 2}
\end{align*}
\begin{align*}
    pos_{World} = \frac{pos_{1} + pos_{2}}{2} - 
    \begin{pmatrix}
        \vec{0} \\
        \vec{0} \\
        \vec{\frac{z_{marker}}{2}}
   \end{pmatrix}
\end{align*}
\begin{align*} yaw_{World} = \frac{yaw_{1} + yaw_{2}}{2}
\end{align*}
As soon as the new world origin is calculated and set, it is possible to determine real-world positions relative to the cargo space and thus, placing virtual packages.

\subsection{Augmenting the World}
\label{subsec:concept-anchor-placement}
In order to augment the world with virtual information, the real world location of the information to display has to be determined. Since for the problem at to be solved, the system already has to know where which pacakge was loaded into the cargo space. The only thing left to do is to create an anchor point at these coordinates. This anchor point can be rediscovered in the video feed of the camera via the world tracking features. When the anchor point is discovered, content can be displayed at this point in the real world. Previously, the world origin was set to the entrance of the cargo space as described in section \ref{fig:concept-world-origin}. This means, that the origin of the real world coordinate system, e.g. the point where
\begin{align*}
    \begin{pmatrix}
        \vec{x}  \\
        \vec{y}  \\
        \vec{z}
    \end{pmatrix} = 
    \begin{pmatrix}
        \vec{0}  \\
        \vec{0}  \\
        \vec{0}
    \end{pmatrix}
\end{align*}
is known to the application. The package position data recorded when loading the packages records all package positions relative to this origin point. Thanks to this, there are no further calculations necessary to know where a package is located. The world tracking feature can measure all distances and can estimate where which feature point discovered is located. With this knowledge, the virtual information can already be placed at the correct location.\newline
As soon as the application knows which package is the package to be unloaded, it creates a real world anchor point at this location. It does not matter whether the camera's field of view currently captures this location. Because of world tracking and the previously performed calibration of the world origin, the application can estimate the location where the package is and show it when the camera's field of view captures the location. \newline
Since world tracking always tracks the surroundings of the smartphone, it also knows at which world coordinate the smartphone itself is located. This makes it easy to calculate the distance $\delta$ between the smartphone and the package:
\begin{align*}
    pos_{p} = 
    \begin{pmatrix}
        \vec{x_{p}}  \\
        \vec{y_{p}}  \\
        \vec{z_{p}}
    \end{pmatrix}
    : \textrm{world coordinates of the package}
\end{align*}
\begin{align*}
    pos_{c} = 
    \begin{pmatrix}
        \vec{x_{c}}  \\
        \vec{y_{c}}  \\
        \vec{z_{c}}
    \end{pmatrix}
    : \textrm{world coordinates of the camera}
\end{align*}

At first, calculate the distance between both points for each dimension:

 $$\Delta x = | \vec{x_{p}} - \vec{x_{c}} |$$
 $$\Delta y = | \vec{y_{p}} - \vec{y_{c}} |$$
 $$\Delta z = | \vec{y_{p}} - \vec{y_{c}} |$$

 Then calculate the distance $\delta$ in 3D space using the Pythagorean theorem:

 $$ \delta = \sqrt{\Delta x^2 + \Delta y^2 + \Delta z^2} $$
The distance $\delta$ can be used by the application to determine whether the package identification process should be started. Since world tracking is active anyways, the distance can be calculated every time world tracking updates it's status. The reason for waiting with the identification until the user is in proximity of the target package is, that the identification process that follows next is very calculation heavy and thus, puts a lot of load on the smartphone's processing units. This drains the battery which is not optimal in a mobile application. Therefore, as soon as the distance $\delta$ falls below the threshold $T$, the package identification starts.
$$ \delta < T : \textrm{user is close enough to start identification process} $$
$$ \delta \geq T : \textrm{user is not close enough to start identification process} $$
As soon as the package identification is started, the user is already in visibility range of the target package. The next step is to locate the actual package in the camera's video feed.
\subsection{Package Identification}
\label{subsec:concept-package-identification}
The smartphone is now in proximity of the target package and it is possible to detect it in the camera's field of view. There are multiple possibilites for this. The most promising possibility is a rectangle detection to detect the surface of the package and then compare visual features of the surface to the actual package data. Other possibilites like a neural network for object detection will not work as well because packages in general are very similar and sometimes obscured and thus, a neural network may have difficulties detecting the whole object. Especially since a package does not have many distinguishable features, only flat surfaces. But usually, at least one surface of the pacakge is visible as a whole. This is either the top side because no package is stacked on top of the target package or the side facing the corridor in the middle of the cargo space where the delivery driver enters. Through to this fact, it is possible to detect this side with the help of a rectangle detector and then extract the surface texture which then can be compared to the actual package data through visual features. Another reason the rectangle detection approach is promising is that rectangle detection algorithms already are in a pretty advanced state compared to neural networks since rectangles are a shape that can be extracted rather easily through visual features. For further details on computer vision and object detection, refer to section \ref{sec:object-detection}.\newline
Another promising approach to identify the packages is to use a barcode or QR-Code detector. This approach has the major advantage that each package can be identified with a certainty of 100\%, because each barcode is unique. The disadvantages are though, that the barcode must be on a side of the package that is visible or the user has to pick up the package, look for the barcode and scan it. But both of these disadvantages can be overcome. Firstmost, it is possible to place every package in a way that the barcode is visible. Secondmost, because of the work done previously in this approach, namely the orientation with the help of markers described in \ref{subsec:concept-registration-markers} and then the display of the estimated position of the package described in \ref{subsec:concept-anchor-placement}. But even when all barcodes are visible, there is one smaller disadvantage. The camera has to be rather close to the target package to be able to identify the barcode. Because of this, the method of barcode detection is implemented as a fallback alongside the rectangle detection approach. The idea is, that in case the rectangle detection fails to detect the package from a distance, the user moves further towards the package looking for the barcode and scanning it. This can then either confirm or deny the choice of package he made according to the estimated position.\newline

\subsubsection{Rectangle Detection}
\label{subsubsec:concept-rectangle-detection}
The concept for the rectangle detection involves a cascade of checks and comparisons to narrow down possible choices. The checks are execuded in a cascaded fashion in descending order of computational power, similar to the object detection algorithm implemented by \citet{Viola-Jones.2001} referenced in chapter \ref{sec:object-detection}. The color comparison is done last since it involves looking at every pixel of an image and comparing the color. The goal is to minify the number of this happening by first eliminating possible detections doing rather simple geometrical checks.
Before the actual checks can be performed though, there has to be a detector to detect real world rectangles in the video feed. Real world rectangle means, that the rectangle must be present in the world coordinate space and not the pixel coordinate space. Due to the translation from 3D into 2D space, rectangular surfaces might appear skewed in the 2D image in picture coordinate space. A good example for this can be seen in figure \ref{fig:yaw-pitch-roll}. \textit{Marker 1} has a rectangular shape in the 3D world coordinate space. In the picture though, the angles are not perpendicular and thus it is not a rectangle in pixel coordinate space. Fortunately there are detection algorithms that are implemented in a way that real world rectangles can be detected in a two dimensional image.\newline
\begin{algorithm}
    \caption{Cascading checks for package detection}
    \label{alg:rectangle-detection}
    \begin{algorithmic}[1]
      \Require{
        \parbox[t]{.89\linewidth}{
            Detected Rectangle $R_{d}$, with colors $colors_{r}$
            \\ target package $P$, with colors $colors_{p}$
            \\ thresholds for position $\tau$, perpendicularities $\sigma$ and lengths $\gamma$
        }
      }
      \Ensure{detected rectangle $R_{d}$ is a surface of $P$ or not}
      \Function{detectPackage}{$R_{d}$, $P$}:
      \State $\delta$ = calculateDistance($R$.position, $P$.position)
      \If{$\delta < \tau$}
        \State $sideVectors$ = calculateSideVectors($corners$)
        \State $perpendicularity$ = comparePerpendicularities($sideVectors$)
        \If{$perpendicularity$ < $\sigma$}
        \State $lengths$ = compareLengths($P$.sides, $sideVectors$)
            \If{$lengths$ < $\gamma$}
                    \If{$colors_{p} == colors_{r}$}
                        \State \Return $true$
                \EndIf
            \EndIf
        \EndIf
      \EndIf
      
      \State \Return $false$
      \EndFunction
    \end{algorithmic}
  \end{algorithm}
When the algorithm detects a real world rectangle, it is still not certain that the detection was correct and whether it is in fact a rectangle or not. Thus, a mechanism to verify the detection is needed. The course of action is described in a strongly simplified way in the pseudo-code in listing \ref{alg:rectangle-detection}. All the steps taken are explained in the following paragraph. Thanks to the world tracking described in section \ref{subsec:concept-registration-markers} and the model of the real world created by the application it is possible to get a 3D world coordinate out of the 2D image for each pixel coordinate. This can be performed for each corner of the detected rectangle to get the coordinates of each corner in world coordinate space.
At first though, in line 2 and 3 of the listing, a check is performed whether the world coordinates of the middle point of the surface is within a certain range of the package position after loading. This is to single out similar packages that were loaded at other positions in the cargo space. The threshold implemented for this check also intrinsically determines the maximum distance a package can be displaced from it's initial location until it can no longer be recognized by the algorithm. The smaller the distance, the lower the probability of detecting another, similar package. But the smaller the distance, the smaller becomes the misplacement threshold. This could lead to a problem later on, because when the threshold is set at a high value, there might be misdetections of similar packages when there are two similar looking packages in close proximity to each other. But when choosing a threshold that is rather low, packages that are not near the expected position might not be detected at all. This can happen when the driver does heavy braking or cornering and the packages slide over the shelf or even fall down to the floor. \newline
The math is similar to the calculation of the distance from the user to the package in \ref{subsec:concept-anchor-placement}.

\begin{align*}
    pos_{p} = 
    \begin{pmatrix}
        \vec{x_{p}}  \\
        \vec{y_{p}}  \\
        \vec{z_{p}}
    \end{pmatrix}
    : \textrm{world coordinates according to package data}
\end{align*}
\begin{align*}
    pos_{c} = 
    \begin{pmatrix}
        \vec{x_{d}}  \\
        \vec{y_{d}}  \\
        \vec{z_{d}}
    \end{pmatrix}
    : \textrm{world coordinates of the detection}
\end{align*}

$$\Delta x = | \vec{x_{p}} - \vec{x_{b}} |$$
$$\Delta y = | \vec{y_{p}} - \vec{y_{b}} |$$
$$\Delta z = | \vec{y_{p}} - \vec{y_{b}} |$$

Then calculate the distance $\delta$ in 3D space using the Pythagorean theorem:

$$ \delta = \sqrt{\Delta x^2 + \Delta y^2 + \Delta z^2} $$

The distance $\delta$ can be used by the application to determine whether it is plausible that the detected rectangle belongs to the target package. In case the distance $\delta$ is smaller the threshold $\tau$, the identification moves on to the next step. Otherwise, the detection is discarded and the process starts from scratch.
$$ \delta < \tau : \textrm{surface belongs to target package} $$
$$ \delta \geq \tau : \textrm{surface does not belong to target package} $$

Now, in order to check whether the detected corners do in fact form a rectangle in real world coordinate space, all sides have to be checked for perpendicularity. Again, since world tracking and it's world coordinates of points or objects is not always completely accurate, the comparison for perpendicularity is done using a threshold $\sigma$. To do this, the vectors of all sides of the rectangle have to be calculated by substracting the two corner vectors:

$$ \vec{c_{topleft}}: \textrm{world coordinates of the top left corner of the detected rectangle} $$
$$ \vec{c_{topright}}: \textrm{world coordinates of the top left corner of the detected rectangle} $$
$$ \vec{c_{bottomleft}}: \textrm{world coordinates of the top left corner of the detected rectangle} $$
$$ \vec{c_{bottomright}}: \textrm{world coordinates of the top left corner of the detected rectangle} $$

$$\vec{s_{top}} = \vec{c_{topleft}} - \vec{c_{topright}}: \textrm{vector of the top side}$$
$$\vec{s_{bottom}} = \vec{c_{bottomleft}} - \vec{c_{bottomright}}: \textrm{vector of the bottom side}$$
$$\vec{s_{left}} = \vec{c_{topleft}} - \vec{c_{bottomleft}}: \textrm{vector of the left side}$$
$$\vec{s_{right}} = \vec{c_{topright}} - \vec{c_{bottomright}}: \textrm{vector of the right side}$$

Then, the dot product can be used to check for perpendicularity. The dot product of two vectors is 0 in case the vectors are perpendicular. Considering the use of the threshold $\sigma$ the dot product has to be smaller than the absolute value of $\sigma$ for the two vectors to be considered perpendicular.

$$\vec{s_{a}} \cdot \vec{s_{b}} < | \sigma | : \textrm{the two vectors are perpendicular}$$
$$\vec{s_{a}} \cdot \vec{s_{b}} > | \sigma | : \textrm{the two vectors are not perpendicular}$$
\newline
This can be seen in the listing in line 4-6. In case the threshold is crossed, detection is a negative, the rectangle is discarded and the identification process starts from the beginning again, searching for rectangles in the video feed. In case the comparison result confirms that the detection was correct, the measurements are validated. Since the application knows the actual measurements of the target package, a validity check can be performed. For this check, the extent of the detected rectangle in real world coordinate space is compared to the three surface areas of the actual package (three, since opposing sides are identical in measurements). And yet again, due to possible inaccuracies, a threshold $\gamma$ is used.

First, calculate the distance $d$ in 3D space for each side vector $\vec{s_{top}}$, $\vec{s_{bottom}}$, $\vec{s_{left}}$ and $\vec{s_{right}}$:
$$ d_{s} = |\vec{s_{s}}|$$
Then, compare each length with the length $l_{p}$, height $h_{p}$ and width $w_{p}$ of the package. Since the left and right side and top and bottom side of a rectangle are always the same length, it is only necessary to compare one of each. So one of each of the following triplets of equations have to be fulfilled for a positive detection:
$$|d_{top} - l_{p}| < \gamma \textrm{ or } |d_{top} - h_{p}| < \gamma  \textrm{ or } |d_{top} - w_{p}| < \gamma$$
$$|d_{left} - l_{p}| < \gamma \textrm{ or } |d_{left} - h_{p}| < \gamma  \textrm{ or } |d_{left} - w_{p}| < \gamma$$

This can be seen in line 8 of the listing. When this check also results in a positive, there is a last feature to compare: The color. Colors vastly differ in different lighting conditions, thus it does not lead to good results when comparing actual color values. In usual RGB color space, there are over 16 million different colors. The probability of getting literally equal colors for an image taken and a reference therefore are almost nonexistent. Because of this, an analyzer is used that determines the four dominant colors of the surface texture out of a limited amount of color segments. To do this, the surface texture has to be extracted from the camera feed at first. Then, it is sent to the color analyzer, which determines the four dominant colors of an image. Because of the already stated reason that color values can vary in different lighting conditions, the values are segmented into color segments. Then, each pixel of the surface texture is analyzed and added to one of those segments. The four segment with the most pixels are the dominant colors. To further reduce the impact of lighting conditions and especially brightness, the color values are transformed from RGB color space to HSV color space. This results in the color not being expressed as separate values for red, green and blue. It is rather expressed by a hue value, a saturation value and a brightness (or value) property. The value property, which essentially is the brightness of the color, is disregarded except for black, white and brown color. This counteracts different lighting conditions to a certain extent. The resulting colors and their values can be seen in \ref{table:color-segments}. These segments were chosen mainly by how a human differentiates between the colors to make it easier to collect package data and to be able to provide the user with additional, textual information about how the package looks.

\begin{table}[ht]
    \centering 
    \begin{tabular}{c c c c} 
    \toprule
    Color & Hue & Saturation & Value \\ [0.5ex]
    \hline
    red & 0 - 10 & 25\% - 70\% & - \\
    orange & 20 - 40 & > 20\% & - \\
    yellow & 40 - 60 & > 20\% & - \\
    green & 60 - 150 & > 20\% & - \\
    turqoise & 150 - 170 & > 20\% & - \\
    lightblue & 170 - 190 & > 20\% & - \\
    darkblue & 190 - 250 & > 20\% & - \\
    purple & 250- 290 & > 20\% & - \\
    pink & 290 - 330 & > 20\% & - \\
    red & 330 - 360 & > 20\% & - \\
    white & - & < 25\% & > 75\% \\
    black & - & - & < 10\% \\
     [1ex] % [1ex] adds vertical space
    \bottomrule
    \end{tabular}
    \caption{Color Segments}
    \label{table:color-segments}
\end{table}

In order for the package to be detected, the colors of the detection has to correspond to the package data. This is done in line 9 of the listing. If this check is also positive, the detection is positive and the target package has been found.

\subsubsection{Barcode Detection}
\label{subsubsec:concept-barcode-detection}
The concept for the barcode detection is rather simple. Since all barcodes have data encoded into them, it is easy to match the data encoded into the barcode with the actual data to identify a package. There are already frameworks that can detect and decode barcodes on their own, so there is no need to implement this again. One thing that will be done to lower computational power necessary, though is to start barcode detection only, when the user is near to the target package. The concept for this has already been described in the context of rectangle detection in section \ref{subsec:concept-anchor-placement}. For barcode detection, this is done in the same manner. In case the barcode detection detects a false positive, a simple validity check is also implemented. Since the real world position of the barcode has to be determined anyway in order to display the position in the \gls{ar} world, this check will be done using this barcode position and position from the package data. This is done in the same fashion as before with the rectangle detection in \ref{subsubsec:concept-rectangle-detection}. \newline

\begin{align*}
    pos_{p} = 
    \begin{pmatrix}
        \vec{x_{p}}  \\
        \vec{y_{p}}  \\
        \vec{z_{p}}
    \end{pmatrix}
    : \textrm{world coordinates according to package data}
\end{align*}
\begin{align*}
    pos_{c} = 
    \begin{pmatrix}
        \vec{x_{b}}  \\
        \vec{y_{b}}  \\
        \vec{z_{b}}
    \end{pmatrix}
    : \textrm{world coordinates of the barcode detection}
\end{align*}

$$\Delta x = | \vec{x_{p}} - \vec{x_{b}} |$$
$$\Delta y = | \vec{y_{p}} - \vec{y_{b}} |$$
$$\Delta z = | \vec{y_{p}} - \vec{y_{b}} |$$

Calculate the distance $\delta$ in 3D space using the Pythagorean theorem:

$$ \delta = \sqrt{\Delta x^2 + \Delta y^2 + \Delta z^2} $$

The distance $\delta$ can be used by the application to determine whether it is plausible that the detected barcode belongs to the target package. In case the distance $\delta$ is smaller the threshold $\rho$, the identification is positive. Otherwise, the detection is discarded and the process starts from scratch.
$$\delta < \rho : \textrm{barcode belongs to target package} $$
$$\delta \geq \rho : \textrm{barcode does not belong to target package} $$

The whole concept for barcode detection can be seen in short form in \ref{alg:barcode-detection}

\begin{algorithm}
    \caption{Cascading checks for package detection}
    \label{alg:barcode-detection}
    \begin{algorithmic}[1]
      \Require{
        \parbox[t]{.89\linewidth}{
            Detected barcode payload $D_{b}$ and position $D_{p}$
            \\ Target package payload $T_{b}$ and position $T_{p}$
            \\ distance threshold $\rho$
        }
      }
      \Ensure{detected barcode $D$ belongs to target package $T$}
      \Function{detectBarcode}{$D_{b}, D_{p}, T_{b}, T_{p}}$:
      \If{$D_{b} = T_{b}$}
        \State $\delta$ = calculateDistance($D_{p}$, $T_{p}$)
        \If{$\delta < \rho$}
            \State \Return $true$
        \EndIf
      \EndIf
      \State \Return $false$
      \EndFunction
    \end{algorithmic}
  \end{algorithm}